import {
  Button,
  Container,
  Grid,
  TextField,
  Typography,
} from "@material-ui/core";
import { useState, useCallback } from "react";
import { useStyles } from "./style";

// Components
import Card from "./components/Card";

function App() {
  const classes = useStyles();
  const [generatedNumbers, setGeneratedNumbers] = useState([]);
  const [currentGeneratedNumber, setCurrentGeneratedNumber] = useState("No numbers were generated yet");
  const [users, setUsers] = useState(5);

  const generateRandomNumber = useCallback(
    () => {
      const randomNumber = Math.floor(Math.random() * 75) + 1;
      if (generatedNumbers.includes(randomNumber)) {
        generateRandomNumber();
      } else {
        setGeneratedNumbers((prev) => [...prev, randomNumber]);
        setCurrentGeneratedNumber(randomNumber);
      }
    },
    // eslint-disable-next-line
    [generatedNumbers]
  );

  return (
    <Container maxWidth="xl" style={{ padding: 20 }}>
      <Grid container spacing={2}>
        <Grid item xs={12} md={3}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Typography variant="h6">Generated Number</Typography>
              <Typography variant="body1">{currentGeneratedNumber}</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6">Users or Cards</Typography>
              <TextField
                value={users}
                type="number"
                onChange={(e) => setUsers(Number(e.target.value))}
              />
            </Grid>
            <Grid item xs={12}>
              <Button
                variant="contained"
                color="primary"
                onClick={generateRandomNumber}
              >
                Generate number
              </Button>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} md={9}>
          <Grid container spacing={2}>
            {Array.from({ length: users }, (_, cardIndex) => (
              <Grid item xs={12} md={6} key={`container-${cardIndex}`}>
                <Card
                  cardIndex={cardIndex}
                  generatedNumbers={generatedNumbers}
                />
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
}

export default App;
