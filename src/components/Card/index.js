import React, { useCallback, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { green } from "@material-ui/core/colors";
import { Button, Grid } from "@material-ui/core";

const useStyles = makeStyles({
  active: {
    background: green[500],
    color: "white",
  },
});

export default function Card(props) {
  const classes = useStyles();
  const [rows, setRows] = useState(() => {
    const numbers = [];

    const getCorrectNumber = (letter, start, end) => {
      const randomNumber =
        Math.floor(Math.random() * (Math.floor(end) - Math.ceil(start) + 1)) +
        Math.ceil(start);
      return numbers.findIndex((stack) => stack[letter] === randomNumber) === -1
        ? randomNumber
        : getCorrectNumber(letter, start, end);
    };

    for (let i = 0; i < 5; i++) {
      numbers.push(
        numbers.length
          ? {
              B: getCorrectNumber("B", 1, 15),
              I: getCorrectNumber("I", 16, 30),
              N: i === 2 ? null : getCorrectNumber("N", 31, 45),
              G: getCorrectNumber("G", 46, 60),
              O: getCorrectNumber("O", 61, 75),
            }
          : {
              B:
                Math.floor(
                  Math.random() * (Math.floor(15) - Math.ceil(1) + 1)
                ) + Math.ceil(1),
              I:
                Math.floor(
                  Math.random() * (Math.floor(30) - Math.ceil(16) + 1)
                ) + Math.ceil(16),
              N:
                Math.floor(
                  Math.random() * (Math.floor(45) - Math.ceil(31) + 1)
                ) + Math.ceil(31),
              G:
                Math.floor(
                  Math.random() * (Math.floor(60) - Math.ceil(46) + 1)
                ) + Math.ceil(46),
              O:
                Math.floor(
                  Math.random() * (Math.floor(75) - Math.ceil(61) + 1)
                ) + Math.ceil(61),
            }
      );
    }

    return numbers;
  });
  const columns = ["B", "I", "N", "G", "O"];

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <TableContainer component={Paper}>
          <Table aria-label="card">
            <TableHead>
              <TableRow>
                {columns.map((c) => (
                  <TableCell align="center" key={c}>
                    {c}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row, index) => (
                <TableRow key={`${props.cardIndex}-${index}`}>
                  <TableCell
                    align="center"
                    {...(props.generatedNumbers.includes(row.B) && {
                      className: classes.active,
                    })}
                  >
                    {row.B}
                  </TableCell>
                  <TableCell
                    align="center"
                    {...(props.generatedNumbers.includes(row.I) && {
                      className: classes.active,
                    })}
                  >
                    {row.I}
                  </TableCell>
                  <TableCell
                    align="center"
                    {...(props.generatedNumbers.includes(row.N) && {
                      className: classes.active,
                    })}
                  >
                    {row.N}
                  </TableCell>
                  <TableCell
                    align="center"
                    {...(props.generatedNumbers.includes(row.G) && {
                      className: classes.active,
                    })}
                  >
                    {row.G}
                  </TableCell>
                  <TableCell
                    align="center"
                    {...(props.generatedNumbers.includes(row.O) && {
                      className: classes.active,
                    })}
                  >
                    {row.O}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      <Grid item xs={12}>
        <Button color="primary" variant="contained">Say Bingo !</Button>
      </Grid>
    </Grid>
  );
}
