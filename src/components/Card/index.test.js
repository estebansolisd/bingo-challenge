import { render, screen } from "@testing-library/react";
import CardComponent from ".";

test("check if the card components can active the generated numbers", () => {
  const mockGeneratedNumbers = Array.from({ length: 75 }, (_, i) => i + 1);
  const { container } = render(
    <CardComponent generatedNumbers={mockGeneratedNumbers} cardIndex={1} />
  );

  let flags = [];
  container.querySelectorAll("td").forEach((td) => {
    flags.push(td.getAttribute("class").includes("active"));
  });
  expect(flags.some((f) => f)).toBe(true);
});
